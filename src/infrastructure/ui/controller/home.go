package controller

import "github.com/labstack/echo"
import "net/http"

// HomeMain ...
func HomeMain(c echo.Context) error {
	return c.String(http.StatusOK, "Hello World")
}

// HomeData ...
func HomeData(c echo.Context) error {
	type data struct {
		First  string `json:"a"`
		Second string `json:"b"`
	}

	a := data{First: "pete", Second: "sampras"}

	return c.JSON(http.StatusOK, a)
}
