package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"bitbucket.org/xavividal/go-workshop/src/infrastructure/ui/controller"
)

func main() {
	e := echo.New()

	e.Static("/", "public")

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", controller.HomeMain)
	e.GET("/data", controller.HomeData)

	e.Logger.Fatal(e.Start(":8080"))
}
