package car

// Cars ...
type Cars struct {
	items map[string]Car
}

// NewCars ...
func NewCars() Cars {
	return Cars{items: make(map[string]Car)}
}

// Count ...
func (c Cars) Count() int {
	return len(c.items)
}

// Add ...
func (c *Cars) Add(nc Car) {
	c.items[nc.Name()] = nc
}

// Copy ...
func (c Cars) Copy() Cars {
	copy := make(map[string]Car)

	for k, v := range c.items {
		copy[k] = v
	}

	return Cars{items: copy}
}
